﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UsersList.Models
{
   public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string EmailAddress { get; set; }
        public int UserTypeId { get; set; }
        public int MaritalStatusTypeId { get; set; }
        public int RaceTypeId { get; set; }
        public int ReligionTypeId { get; set; }
        public string DateOfBirth { get; set; }
        public long Salary { get; set; }
        public string UserNote { get; set; }
        public string AvatarUrl { get; set; }
        public string AvatarThumbUrl { get; set; }
        public int HiddenBy { get; set; }
        public bool IsHidden { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public int UpdatedBy { get; set; }

    }
}
