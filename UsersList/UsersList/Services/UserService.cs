﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace UsersList.Services
{
    class UserService
    {
        public async Task<JsonValue> GetRequestAsync(string Url)
        {
            //// Create an HTTP web request using the URL:
            //HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(Url));
            ////byte[] postBytes = Encoding.ASCII.GetBytes(Url);
            ////request.ContentLength = postBytes.Length;
            //request.ContentType = "application/json";
            //request.Method = "POST";

            //// Send the request to the server and wait for the response:
            //using (WebResponse response = await request.GetResponseAsync())
            //{
            //    // Get a stream representation of the HTTP web response:
            //    using (Stream stream = response.GetResponseStream())
            //    {
            //        // Use this stream to build a JSON document object:
            //        JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));

            //        // Return the JSON document:
            //        return jsonDoc;
            //    }
            //}
            HttpClient client = new HttpClient();
            Uri uri = new Uri(Url);
            var content = new StringContent("", Encoding.UTF8, "application/json");
            var response = await client.GetAsync(uri);
             var result = JsonValue.Load(response.Content.ReadAsStreamAsync().Result);
            return result;
        }
        public async Task<JsonValue> PostRequestAsync(string Url)
        {
            HttpClient client = new HttpClient();
            Uri uri = new Uri(Url);
            var content = new StringContent("", Encoding.UTF8, "application/json");
            var response = await client.PostAsync(uri, content);
            var result = JsonValue.Load(response.Content.ReadAsStreamAsync().Result);
            return result;
        }
    }
}
