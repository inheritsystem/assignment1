﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersList.Models;
using UsersList.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UsersList.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddOrEditUser : ContentPage
	{
        User user = new User();
		public AddOrEditUser (User u)
		{
			InitializeComponent ();
            user = u;
            if(u.Id > 0)
            {
                SaveOrEdit.Text = "Update";
            }
            FirstName.Text = user.FirstName;
            LastName.Text = user.LastName;
            Password.Text = user.Password;
            //DOB.Date = Convert.ToDateTime(user.DateOfBirth);
            Email.Text = user.EmailAddress;
            RaceTypeId.Text = user.RaceTypeId.ToString();
            ReligionTypeId.Text = user.ReligionTypeId.ToString();
            Salary.Text = user.Salary.ToString();
            user.UpdatedBy = Convert.ToInt32(UpdatedBy.Text);
            UpdatedBy.Text = user.UpdatedBy.ToString();
            UserNote.Text = user.UserNote;
            UserTypeID.Text = user.UserTypeId.ToString();
            MaritalStatusTypeId.Text = user.MaritalStatusTypeId.ToString();
            UpdatedAt.Text = user.UpdatedAt;
            CreatedAt.Text = user.CreatedAt;
            CreatedBy.Text = user.CreatedBy.ToString();
        }
        public AddOrEditUser()
        {
            InitializeComponent();
        }
        private void Save(object sender, EventArgs e)
        {
                user.FirstName = FirstName.Text;
                user.LastName = LastName.Text;
                user.Username = UserName.Text;
                user.Password = Password.Text;
                user.DateOfBirth = DOB.Date.ToLongDateString();
                user.EmailAddress = Email.Text;
                user.RaceTypeId = Convert.ToInt32(RaceTypeId.Text);
                user.ReligionTypeId = Convert.ToInt32(ReligionTypeId.Text);
                user.Salary = Convert.ToInt64(Salary.Text);
                user.UpdatedBy = Convert.ToInt32(UpdatedBy.Text);
                user.UserNote = UserNote.Text;
                user.UserTypeId = Convert.ToInt32(UserTypeID.Text);
                user.MaritalStatusTypeId = Convert.ToInt32(MaritalStatusTypeId.Text);
                user.UpdatedAt = UpdatedAt.Text;
                user.CreatedAt = CreatedAt.Text;
                user.CreatedBy =CreatedBy.Text;
            User_ViewModel um = new User_ViewModel();
           
            if (user.Id>0)
            {
                um.EditUser(user);
                Navigation.PushModalAsync(new NavigationPage(new ListPage()));

            }
            else
            {
                um.AddUser(user);
                Navigation.PushModalAsync(new NavigationPage(new ListPage()));
            }
        }
    }
}