﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersList.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UsersList.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListDetail : ContentPage
	{

        User user1 = new User();
        public ListDetail(User user)
		{
            InitializeComponent();
            BindingContext = user;
            ListGrid.Children.Add(new Label { Text = user.FirstName }, 1, 0);
            ListGrid.Children.Add(new Label { Text = user.LastName }, 1, 1);
            ListGrid.Children.Add(new Label { Text = user.Username }, 1, 2);
            ListGrid.Children.Add(new Label { Text = user.Password }, 1, 3);
            ListGrid.Children.Add(new Label { Text = user.EmailAddress }, 1, 4);
            ListGrid.Children.Add(new Label { Text = user.MaritalStatusTypeId.ToString() }, 1, 5);
            ListGrid.Children.Add(new Label { Text = user.RaceTypeId.ToString() }, 1, 6);
            ListGrid.Children.Add(new Label { Text = user.DateOfBirth }, 1, 7);
            ListGrid.Children.Add(new Label { Text = user.ReligionTypeId.ToString() }, 1, 8);
            ListGrid.Children.Add(new Label { Text = user.Salary.ToString() }, 1, 9);
            ListGrid.Children.Add(new Label { Text = user.UserNote }, 1, 10);
            ListGrid.Children.Add(new Label { Text = user.CreatedBy }, 1, 11);
            ListGrid.Children.Add(new Label { Text = ConvertMilliSecondsToDateTime(user.CreatedAt) }, 1, 12);
            ListGrid.Children.Add(new Label { Text = ConvertMilliSecondsToDateTime(user.UpdatedAt) }, 1, 13);

            user1 = user;

        }
        private void Edit(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new Views.AddOrEditUser(user1));
        }
        private string ConvertMilliSecondsToDateTime(string s)
        {
            string ss = s.Substring(6, 13);
            DateTimeOffset dateTime = DateTimeOffset.FromUnixTimeMilliseconds(Convert.ToInt64(ss));
            DateTime dt = dateTime.DateTime;
            return dt.ToString();
        }
    }
}