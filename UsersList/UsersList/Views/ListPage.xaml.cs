﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsersList.Models;
using UsersList.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UsersList.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListPage : ContentPage
	{
		public ListPage ()
		{
			InitializeComponent ();
            LoadUsers();
            UsersList.ItemSelected += (sender, e) => {
               Navigation.PushModalAsync(new ListDetail(((ListView)sender).SelectedItem as User));
            };

        }
        public async void LoadUsers()
        {
            User_ViewModel vm = new User_ViewModel();
            UsersList.ItemsSource = await vm.LoadListAsync();

        }
        protected void ListItems_Refreshing(object s,EventArgs e)
        {
            LoadUsers();
            UsersList.EndRefresh();
        }
        public void Add_new(object s,EventArgs e)
        {
            Navigation.PushModalAsync(new AddOrEditUser());
        }



    }
}