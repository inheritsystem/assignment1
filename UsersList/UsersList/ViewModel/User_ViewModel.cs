﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Json;
using System.Text;
using System.Threading.Tasks;
using UsersList.Models;
using UsersList.Services;

namespace UsersList.ViewModel
{
    class User_ViewModel
    {
        public async Task<List<User>> LoadListAsync()
        {
            UserService service = new UserService();
            string RequestUrl = "http://shreejiadultdaycare.azurewebsites.net/user/User?cmd=list";
            JsonValue json = await service.GetRequestAsync(RequestUrl);
            var list = json["list"].ToString();
            List<User> users = new List<User>();

            //var ls = JsonConvert.DeserializeObject(json);
            try
            {
                 users = new List<User>(JsonConvert.DeserializeObject<List<User>>(list));
                
            }
            catch (Exception ex)
            {
                
            }
            return users;
        }
        public async void AddUser(User u)
        {
            UserService service = new UserService();
            string RequestUrl = "http://shreejiadultdaycare.azurewebsites.net/user/User?cmd=save&FirstName="+u.FirstName+"&LastName="+u.LastName+ "&Username="+u.Username + "&Password=" + u.Password + "&EmailAddress=" + u.EmailAddress + "&UserTypeId=" + u.UserTypeId + "&MaritalStatusTypeId=" + u.MaritalStatusTypeId + "&RaceTypeId=" + u.RaceTypeId + "&ReligionTypeId=" + u.ReligionTypeId + "&DateOfBirth=" + u.DateOfBirth + "&Salary=" + u.Salary + "&UserNote=" + u.UserNote + "&AvatarUrl=" + u.AvatarUrl + "&AvatarThumbUrl=" + u.AvatarThumbUrl + "&HiddenBy=" + u.HiddenBy + "&IsHidden=" + u.IsHidden + "&CreatedBy=" + u.CreatedBy + "&CreatedAt=" + u.CreatedAt + "&UpdatedAt=" + u.UpdatedAt + "&UpdatedBy=" + u.UpdatedBy;
            var json = await service.PostRequestAsync(RequestUrl);
          
        }
        public async void EditUser(User u)
        {
            UserService service = new UserService();
            string RequestUrl = "http://shreejiadultdaycare.azurewebsites.net/user/User?cmd=save&FirstName=" + u.FirstName + "&LastName=" + u.LastName + "&Username=" + u.Username + "&Password=" + u.Password + "&EmailAddress=" + u.EmailAddress + "&UserTypeId=" + u.UserTypeId + "&MaritalStatusTypeId=" + u.MaritalStatusTypeId + "&RaceTypeId=" + u.RaceTypeId + "&ReligionTypeId=" + u.ReligionTypeId + "&DateOfBirth=" + u.DateOfBirth + "&Salary=" + u.Salary + "&UserNote=" + u.UserNote + "&AvatarUrl=" + u.AvatarUrl + "&AvatarThumbUrl=" + u.AvatarThumbUrl + "&HiddenBy=" + u.HiddenBy + "&IsHidden=" + u.IsHidden + "&CreatedBy=" + u.CreatedBy + "&CreatedAt=" + u.CreatedAt + "&UpdatedAt=" + u.UpdatedAt + "&UpdatedBy=" + u.UpdatedBy+"&Id="+u.Id;
            var json = await service.PostRequestAsync(RequestUrl);
        }
    }
}
