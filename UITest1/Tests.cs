﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITest1
{
    [TestFixture(Platform.Android)]
  //  [TestFixture(Platform.iOS)]
    public class Tests
    {
        IApp app;
        Platform platform;

        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        [Test]
        public void AppLaunches()
        {
            app.Screenshot("First screen.");
        }



        [Test]
        public void Scroll_down()
        {
            app.ScrollDownTo("sam");

        }
        [Test]
        public void Scroll_down1()
        {
            app.ScrollDownTo("Test");

        }
        [Test]
        public void Tap_jack1()
        {
            app.Tap(x => x.Marked("New"));
            app.Back();
            app.Tap(x => x.Marked("New"));
            app.ScrollDown();
            app.Tap(x => x.Marked("Save"));
            app.Tap(x => x.Marked("tom"));


            app.ScrollDown();
            app.Tap(x => x.Marked("Edit"));

            var fail = app.Query(x => x.Marked("tom"));
            Assert.IsTrue(fail != null, "Nothing to display");

           
        }



        [Test]
        public void Tap_back1()
        {
            app.Tap(x => x.Marked("bill"));

            app.ScrollDown();

            app.Tap(x => x.Marked("Edit"));

            var fail = app.Query(x => x.Marked("bill"));
            Assert.IsTrue(fail != null, "Nothing to display");

            app.Back();

            app.Back();

            var fail1 = app.Query(x => x.Marked("bill"));
            Assert.IsTrue(fail1 != null, "Nothing to display");

        }

        [Test]
        public void NewTest()
        {
            app.Tap(x => x.Marked("New"));
            app.Tap(x => x.Class("FormsEditText"));
            app.EnterText(x => x.Class("FormsEditText"), "Harish");
            app.Tap(x => x.Class("FormsEditText").Index(1));
            app.EnterText(x => x.Class("FormsEditText").Index(1), "N");
            app.Tap(x => x.Class("FormsEditText").Index(2));
            app.EnterText(x => x.Class("FormsEditText").Index(2), "Pav_n");
            app.Tap(x => x.Class("FormsEditText").Index(3));
            app.EnterText(x => x.Class("FormsEditText").Index(3), "12345");
            app.Tap(x => x.Class("FormsEditText").Index(4));
            app.EnterText(x => x.Class("FormsEditText").Index(4), "Vcis0@gmail.com");
            app.Tap(x => x.Class("FormsEditText").Index(5));
            app.EnterText(x => x.Class("FormsEditText").Index(5), "122");
            app.Tap(x => x.Class("FormsEditText").Index(6));
            app.EnterText(x => x.Class("FormsEditText").Index(6), "0");
            app.Tap(x => x.Text("EmailAddress"));

            app.PressEnter();
            app.Tap(x => x.Class("FormsEditText").Index(7));
            app.EnterText(x => x.Class("FormsEditText").Index(7), "1");
            app.Tap(x => x.Text("Race Type Id"));
            app.PressEnter();
            app.Tap(x => x.Class("FormsEditText").Index(8));
            app.EnterText(x => x.Class("FormsEditText").Index(8), "01");
            app.PressEnter();
            app.Tap(x => x.Class("FormsEditText").Index(9));
            app.EnterText(x => x.Class("FormsEditText").Index(9), "10000");
            app.PressEnter();
            app.ScrollDownTo("User Note");
            app.ScrollDown();
            app.Tap(x => x.Class("FormsEditText").Index(4));
            app.EnterText(x => x.Class("FormsEditText").Index(4), "ok");
           // app.Tap(x => x.Text("Race Type Id"));
            app.PressEnter();
            app.Tap(x => x.Class("FormsEditText").Index(5));
            app.EnterText(x => x.Class("FormsEditText").Index(5), "inherit");
            app.PressEnter();
            app.Tap(x => x.Class("FormsEditText").Index(6));
            app.EnterText(x => x.Class("FormsEditText").Index(6), "Wework");
            app.PressEnter();

            app.Tap(x => x.Class("FormsEditText").Index(7));
            app.EnterText(x => x.Class("FormsEditText").Index(7), "today");
            app.Tap(x => x.Text("Race Type Id"));
            app.PressEnter();
            app.Tap(x => x.Class("FormsEditText").Index(8));
            app.EnterText(x => x.Class("FormsEditText").Index(8), "vishrut");
            app.PressEnter();
         
            app.Tap(x => x.Text("Save"));

           


        }


        [Test]
        public void single_Save9()
        {


            app.Tap(x => x.Marked("New"));

            app.Tap(x => x.Class("FormsEditText"));
            app.EnterText(x => x.Class("FormsEditText"), "paaaaa");

            app.Tap(x => x.Class("FormsEditText").Index(1));
            app.EnterText(x => x.Class("FormsEditText").Index(1), "Nwfdyswd");

            app.Tap(x => x.Class("FormsEditText").Index(2));
            app.EnterText(x => x.Class("FormsEditText").Index(2), "Pav_n");

            app.Tap(x => x.Class("FormsEditText").Index(4));
            app.EnterText(x => x.Class("FormsEditText").Index(4), "Vcis0@gmail.com");

            app.Tap(x => x.Class("FormsEditText").Index(3));
            app.EnterText(x => x.Class("FormsEditText").Index(3), "12345");
            app.PressEnter();

            app.ScrollDownTo("User Note");

            app.Tap(x => x.Marked("User Note"));

            app.Tap(x => x.Marked("User Note").Index(1));
            app.EnterText("Pagdhsajg");
            app.PressEnter();
            app.Tap(x => x.Marked("CreatedBy"));
            app.Tap(x => x.Marked("CreatedBy").Index(1));
            app.EnterText("Inherit");
            app.PressEnter();

            app.Back();

            app.Back();

            var fail = app.Query(x => x.Marked("Pavan"));
            Assert.IsTrue(fail != null, "Nothing to display");
        }


        [Test]
        public void Tap_back2bac()
        {
            app.Tap(x => x.Marked("Pavan"));

            app.ScrollDown();

            app.Tap(x => x.Marked("Edit"));

            app.Back();

            app.Tap(x => x.Marked("Edit"));

            app.Back();

            var fail = app.Query(x => x.Marked("Pavan"));
            Assert.IsTrue(fail != null, "Nothing to display");

            app.Back();


        }


        [Test]
        public void Scroll_updown3()
        {
            app.ScrollDownTo("Test");

            app.WaitForElement(c => c.Marked("Test"));
            app.Tap(x => x.Marked("Test").Index(1));
            app.Back();
            app.Back();

            app.ScrollUp();

            app.ScrollUpTo("tom");

            app.WaitForElement(c => c.Marked("tom"));
            app.Tap(x => x.Marked("tom"));

            app.Back();
            app.Back();

          
        }




        [Test]
    public void Tap_Test2()
    {
            app.ScrollDown();
        app.ScrollDownTo("Test");

        app.WaitForElement(c => c.Marked("Test"));
        app.Tap(x => x.Marked("Test").Index(1));

        var fail = app.Query(x => x.Marked("Kaushik@yahoo.com"));
        Assert.IsTrue(fail != null, "Nothing to display");



    }

    [Test]
    public void Tap_Test11()
    {
        app.ScrollDownTo("Test");

       
        app.Tap(x => x.Marked("Test").Index(0));

        app.ScrollDown();

            app.ScrollDownTo(x => x.Marked("UpdatedBy"));
        

        app.Tap(x => x.Marked("Edit"));

        var fail = app.Query(x => x.Marked("User"));
        Assert.IsTrue(fail != null, "Nothing to display");

    }

    [Test]
    public void Scroll_updown22()
    {
        app.ScrollDownTo("Test");

        app.WaitForElement(c => c.Marked("Test"));
        app.Tap(x => x.Marked("Test").Index(1));
        
        app.Back();
            app.Back();
            app.ScrollUp();

        app.ScrollUpTo("Pavan");

        app.WaitForElement(c => c.Marked("tom"));
        app.Tap(x => x.Marked("tom"));

    }


        [Test]
        public void single_Save21()
        {


            app.Tap(x => x.Marked("New"));

            app.Tap(x => x.Class("FormsEditText"));
            app.EnterText(x => x.Class("FormsEditText"), "46464");

            app.Tap(x => x.Class("FormsEditText").Index(1));
            app.EnterText(x => x.Class("FormsEditText").Index(1), "N&^&$%%^#");
            app.PressEnter();
            app.ScrollDownTo("User Note");

            app.Tap(x => x.Marked("Save"));

           

            
        }

        [Test]
        public void single_Save09()
        {


            app.Tap(x => x.Marked("New"));

            app.Tap(x => x.Class("FormsEditText"));
            app.EnterText(x => x.Class("FormsEditText"), "sdfhsg");

            app.Tap(x => x.Class("FormsEditText").Index(1));
            app.EnterText(x => x.Class("FormsEditText").Index(1), "000N");

            app.Tap(x => x.Class("FormsEditText").Index(2));
            app.EnterText(x => x.Class("FormsEditText").Index(2), "Pav_ngsdjhfgs");

            app.Tap(x => x.Class("FormsEditText").Index(4));
            app.EnterText(x => x.Class("FormsEditText").Index(4), "Vfsdyfjg@gmail.com");

            app.Tap(x => x.Class("FormsEditText").Index(3));
            app.EnterText(x => x.Class("FormsEditText").Index(3), "1sdfsdhfgsdjfgsufydfauygt7u6tfwdgweuyrt");
            app.PressEnter();

            app.ScrollDownTo("User Note");

            app.Tap(x => x.Marked("User Note"));

            app.Tap(x => x.Marked("User Note").Index(1));
            app.EnterText("Pagdhsajg");
            app.PressEnter();
            app.Tap(x => x.Marked("CreatedBy"));
            app.Tap(x => x.Marked("CreatedBy").Index(1));
            app.EnterText("00000000000");
            app.PressEnter();

            app.Back();

            app.Back();

            var fail = app.Query(x => x.Marked("Pavan"));
            Assert.IsTrue(fail != null, "Nothing to display");
        }










    }
}

